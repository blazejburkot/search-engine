package com.example.searchengine.console.cmd;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class CommandParserTest {
    private final CommandParser parser = new CommandParser();

    @ParameterizedTest
    @CsvSource({"to be,SEARCH", ",EMPTY",  "' ',EMPTY", "'',EMPTY", ":quit,EXIT"})
    public void dataDrivenTest(final String input, final String expectedCommandType) {
        // when
        final Command command = parser.parseCommand(input);
        // then
        assertThat(command.getType().name()).isEqualTo(expectedCommandType);
    }
}
