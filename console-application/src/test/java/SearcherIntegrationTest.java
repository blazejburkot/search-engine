import com.example.searchengine.console.util.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

class SearcherIntegrationTest {
    private static final InputStream DEFAULT_STDIN = System.in;
    private static final PrintStream DEFAULT_STOUT= System.out;

    @AfterEach
    public void rollbackChangesToStdin() {
        System.setIn(DEFAULT_STDIN);
        System.setOut(DEFAULT_STOUT);
    }

    @Test
    public void shouldExitWhenGivenDirectoryDoesNotExist() {
        Searcher.main(new String[]{"/foo/bar/not/exist"});
    }

    @Test
    public void sampleScenario() {
        // given
        final String dataDirectory = FileUtils.resourceNameToPath("testdata").toAbsolutePath().toString();

        System.setIn(new ByteArrayInputStream("Lorem\n:quit\n".getBytes()));

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        final PrintStream outputStream = new PrintStream(out);
        System.setOut(outputStream);

        // when
        Searcher.main(new String[]{dataDirectory});

        // then
        System.setOut(DEFAULT_STOUT);
        final String consoleOutput = out.toString();
        System.out.println("---\n" + consoleOutput + "\n---");
        assertThat(consoleOutput).contains("lorem-ipsum-a1.txt : 100%");
    }
}
