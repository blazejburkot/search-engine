package com.example.searchengine.console.cmd.impl;

public final class ExitCommand extends EmptyCommand  {

    @Override
    public CommandType getType() {
        return CommandType.EXIT;
    }
}
