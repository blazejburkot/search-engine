package com.example.searchengine.console.cmd.impl;

import com.example.searchengine.config.ApplicationContext;
import com.example.searchengine.console.cmd.Command;

public class EmptyCommand implements Command {

    @Override
    public void execute(final ApplicationContext applicationContext) {
    }

    @Override
    public CommandType getType() {
        return CommandType.EMPTY;
    }
}
