package com.example.searchengine.console.cmd.impl;

import com.example.searchengine.SearchEngineFacade;
import com.example.searchengine.config.ApplicationContext;
import com.example.searchengine.console.cmd.Command;
import com.example.searchengine.rater.ResourceDataScore;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SearchCommand implements Command {
    private final String query;

    public SearchCommand(final String query) {
        this.query = query;
    }

    @Override
    public void execute(final ApplicationContext applicationContext) {
        final SearchEngineFacade searchEngineFacade = applicationContext.getBean(SearchEngineFacade.class);

        final List<ResourceDataScore> scores = searchEngineFacade.rateResources(query)
                .stream()
                .filter(dataScore -> dataScore.getScore() > 0)
                .sorted(Comparator.comparingInt(ResourceDataScore::getScore).reversed())
                .collect(Collectors.toCollection(ArrayList::new));

        if (scores.size() == 0) {
            System.out.println("no matches found");
        } else {
            scores.forEach(score ->
                    System.out.println(String.format("%s : %s%%", score.getResourceName(), score.getScore())));
        }
    }

    @Override
    public CommandType getType() {
        return CommandType.SEARCH;
    }
}
