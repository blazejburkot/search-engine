package com.example.searchengine.console.cmd;

import com.example.searchengine.console.cmd.impl.EmptyCommand;
import com.example.searchengine.console.cmd.impl.ExitCommand;
import com.example.searchengine.console.cmd.impl.SearchCommand;

public class CommandParser {

    public Command parseCommand(final String input) {
        if (input == null || input.trim().length() == 0) {
            return new EmptyCommand();
        }

        if (":quit".equalsIgnoreCase(input)) {
            return new ExitCommand();
        }

        return new SearchCommand(input);
    }
}
