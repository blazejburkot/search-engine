package com.example.searchengine.console;

import com.example.searchengine.SearchEngineFacade;
import com.example.searchengine.config.ApplicationContext;
import com.example.searchengine.console.cmd.Command;
import com.example.searchengine.console.cmd.CommandParser;
import com.example.searchengine.console.cmd.impl.CommandType;

import java.util.Scanner;
import java.util.logging.Logger;

public class ConsoleApplication {
    private static final Logger LOGGER = Logger.getLogger(ConsoleApplication.class.getCanonicalName());

    private final CommandParser commandParser;
    private final ApplicationContext applicationContext;
    private final SearchEngineFacade searchEngineFacade;

    public ConsoleApplication(final ApplicationContext appContext) {
        this.commandParser = new CommandParser();
        this.applicationContext = appContext;
        this.searchEngineFacade = applicationContext.getBean(SearchEngineFacade.class);
    }

    public void run(final String directoryPath) {
        final boolean success = loadData(directoryPath);
        if (!success) {
            return;
        }

        processUserInput();
    }

    private void processUserInput() {
        final Scanner scanner = new Scanner(System.in);
        Command command;
        do {
            System.out.print("search> ");
            final String userInput = scanner.nextLine();

            command = commandParser.parseCommand(userInput);
            executeCommand(command);
        }
        while (command != null && command.getType() != CommandType.EXIT);
    }

    private void executeCommand(final Command command) {
        LOGGER.info("Executing " + command);
        try {
            command.execute(applicationContext);
        } catch (Exception ex) {
            LOGGER.severe("Got unexpected exception: " + ex.getMessage());
            System.err.println("Got unexpected exception: " + ex.getMessage());
        }
    }

    public boolean loadData(final String directoryPath) {
        try {
            searchEngineFacade.readDataFromDirectory(directoryPath);
            System.out.println(String.format("%d files read is directory %s",
                    searchEngineFacade.getNumberOfResources(), directoryPath));
            return true;
        } catch (IllegalArgumentException ex) {
            LOGGER.info(ex.getMessage());
            System.err.println(ex.getMessage());
        } catch (Exception ex) {
            LOGGER.severe("Got unexpected exception: " + ex.getMessage());
            System.err.println("Got unexpected exception: " + ex.getMessage());
        }
        return false;
    }
}
