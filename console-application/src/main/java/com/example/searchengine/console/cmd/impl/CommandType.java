package com.example.searchengine.console.cmd.impl;

public enum CommandType {
    EMPTY,
    SEARCH,
    EXIT
}
