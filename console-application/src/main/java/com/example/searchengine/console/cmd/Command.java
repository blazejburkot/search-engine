package com.example.searchengine.console.cmd;

import com.example.searchengine.config.ApplicationContext;
import com.example.searchengine.console.cmd.impl.CommandType;

public interface Command {
    void execute(ApplicationContext applicationContext);
    CommandType getType();
}
