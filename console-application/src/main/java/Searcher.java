import com.example.searchengine.config.ApplicationContext;
import com.example.searchengine.config.LoggerConfiguration;
import com.example.searchengine.config.SearchEngineConfiguration;
import com.example.searchengine.console.ConsoleApplication;

import java.util.List;
import java.util.function.Consumer;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class Searcher {

    public static void main(final String[] args) {
        if (args.length != 1) {
            System.out.println("Usage:\n   java -cp SimpleSearch.jar Searcher <data directory path>");
            System.exit(1);
        }

        final ApplicationContext appContext = createAppContext();
        final ConsoleApplication application = new ConsoleApplication(appContext);
        application.run(args[0]);
    }

    private static ApplicationContext createAppContext() {
        final List<Consumer<ApplicationContext>> configs = List.of(
                LoggerConfiguration::configureLogger,
                SearchEngineConfiguration::configureModule
        );
        return ApplicationContext.createContext(configs);
    }
}
