<?xml version="1.0" encoding="UTF-8"?>
<project xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns="http://maven.apache.org/POM/4.0.0"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.example.searchengine</groupId>
    <artifactId>search-engine-parent</artifactId>
    <version>1.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <modules>
        <module>search-engine-core</module>
        <module>console-application</module>
    </modules>

    <properties>
        <!-- Sources encoding -->
        <encoding>UTF-8</encoding>
        <project.build.sourceEncoding>${encoding}</project.build.sourceEncoding>
        <project.reporting.outputEncoding>${encoding}</project.reporting.outputEncoding>

        <!-- Java sources version -->
        <java.version>12</java.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>

        <!-- Plugins config -->
        <pmd.printFailingErrors>true</pmd.printFailingErrors>
        <checkstyle.consoleOutput>true</checkstyle.consoleOutput>
        <checkstyle.config.location>checkstyle-rules.xml</checkstyle.config.location>
        <checkstyle.suppressions.location>checkstyle-suppressions.xml</checkstyle.suppressions.location>

        <!-- Plugins versions -->
        <maven-clean-plugin.version>3.1.0</maven-clean-plugin.version>
        <maven-compiler-plugin.version>3.7.0</maven-compiler-plugin.version>
        <versions-maven-plugin.version>2.7</versions-maven-plugin.version>
        <maven-source-plugin.version>3.0.1</maven-source-plugin.version>
        <git-commit-id-plugin.version>3.0.0</git-commit-id-plugin.version>
        <maven-pmd-plugin.version>3.12.0</maven-pmd-plugin.version>
        <maven-checkstyle-plugin.version>3.1.0</maven-checkstyle-plugin.version>
        <maven-surefire-plugin.version>2.22.0</maven-surefire-plugin.version>
        <jacoco-maven-plugin.version>0.8.2</jacoco-maven-plugin.version>
        <maven-jxr-plugin.version>3.0.0</maven-jxr-plugin.version>
        <spring-boot.version>2.3.4.RELEASE</spring-boot.version>

        <!-- Dependencies versions -->
        <assertj.version>3.17.2</assertj.version>
        <mockito-core.version>3.5.13</mockito-core.version>
        <junit-jupiter.version>5.7.0</junit-jupiter.version>
    </properties>

    <dependencies>
        <!-- TEST DEPENDENCIES -->
        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>${assertj.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>${mockito-core.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <!-- The Compiler Plugin is used to compile the sources      -->
            <!-- https://maven.apache.org/plugins/maven-compiler-plugin/ -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <showDeprecation>true</showDeprecation>
                    <showWarnings>true</showWarnings>
                </configuration>
            </plugin>

            <!-- The Versions Plugin is used to manage the versions of artifacts in a project's POM. -->
            <!-- https://www.mojohaus.org/versions-maven-plugin/                                     -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>versions-maven-plugin</artifactId>
                <version>${versions-maven-plugin.version}</version>
                <configuration>
                    <processDependencyManagement>false</processDependencyManagement>
                    <processPluginDependenciesInPluginManagement>true</processPluginDependenciesInPluginManagement>
                </configuration>
            </plugin>

            <!-- The Source Plugin creates a jar archive of the source files of the current project -->
            <!-- https://maven.apache.org/plugins/maven-source-plugin/                              -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>${maven-source-plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- The Spring Boot Plugin generates file with build information                          -->
            <!-- https://docs.spring.io/spring-boot/docs/current/maven-plugin/examples/build-info.html -->
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>${spring-boot.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>build-info</goal>
                        </goals>
                        <configuration>
                            <additionalProperties>
                                <java.version>${java.version}</java.version>
                            </additionalProperties>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- The Git Commit Id Plugin generates file with git commit information -->
            <!-- https://github.com/git-commit-id/maven-git-commit-id-plugin         -->
            <plugin>
                <groupId>pl.project13.maven</groupId>
                <artifactId>git-commit-id-plugin</artifactId>
                <version>${git-commit-id-plugin.version}</version>
                <executions>
                    <execution>
                        <id>git-information</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>revision</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <failOnNoGitDirectory>false</failOnNoGitDirectory>
                    <failOnUnableToExtractRepoInfo>false</failOnUnableToExtractRepoInfo>
                    <prefix>git</prefix>
                    <skipPoms>false</skipPoms>
                    <excludeProperties>
                        <excludeProperty>git.user.*</excludeProperty>
                    </excludeProperties>
                    <generateGitPropertiesFile>true</generateGitPropertiesFile>
                    <generateGitPropertiesFilename>${project.build.outputDirectory}/META-INF/git-info.properties
                    </generateGitPropertiesFilename>
                </configuration>
            </plugin>

            <!-- The PMD Plugin performs static code analysis       -->
            <!-- https://maven.apache.org/plugins/maven-pmd-plugin/ -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-pmd-plugin</artifactId>
                <version>${maven-pmd-plugin.version}</version>
                <configuration>
                    <aggregate>true</aggregate>
                    <failOnViolation>true</failOnViolation>
                    <targetJdk>${java.version}</targetJdk>
                    <excludes>
                        <exclude>**/generated/**/*.java</exclude>
                    </excludes>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                            <goal>cpd-check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- The JaCoCo Plugin generates code coverage report, which can be found in  `target/site/jacoco/index.html` directory -->
            <!-- https://www.eclemma.org/jacoco/trunk/doc/maven.html                                                                -->
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco-maven-plugin.version}</version>
                <configuration>
                    <destFile>${basedir}/target/jacoco.exec</destFile>
                    <dataFile>${basedir}/target/jacoco.exec</dataFile>
                </configuration>
                <executions>
                    <execution>
                        <id>jacoco-prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>jacoco-generate-coverage-report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- The Checkstyle Plugin performs static code analysis       -->
            <!-- https://maven.apache.org/plugins/maven-checkstyle-plugin/ -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <version>${maven-checkstyle-plugin.version}</version>
                <configuration>
                    <failOnViolation>false</failOnViolation>
                    <failsOnError>true</failsOnError>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jxr-plugin</artifactId>
                <version>${maven-jxr-plugin.version}</version>
            </plugin>
        </plugins>
    </reporting>
</project>

