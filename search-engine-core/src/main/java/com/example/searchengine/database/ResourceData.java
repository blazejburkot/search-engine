package com.example.searchengine.database;

import com.example.searchengine.model.UserQuery;
import com.example.searchengine.rater.ResourceDataRater;
import com.example.searchengine.rater.ResourceDataScore;

public interface ResourceData {
    String getName();
    void appendWord(String word);
    boolean containsWord(String word);

    ResourceDataScore accept(ResourceDataRater rater, UserQuery query);
}
