package com.example.searchengine.database;

import com.example.searchengine.model.UserQuery;
import com.example.searchengine.rater.ResourceDataRater;
import com.example.searchengine.rater.ResourceDataScore;

import java.util.HashSet;
import java.util.Set;

public class ResourceWordsSet implements ResourceData {
    private final String name;
    private final Set<String> words;

    public ResourceWordsSet(final String name) {
        this.name = name;
        this.words = new HashSet<>();
    }

    @Override
    public ResourceDataScore accept(final ResourceDataRater rater, final UserQuery query) {
        return rater.rate(this, query);
    }

    @Override
    public void appendWord(final String word) {
        words.add(word);
    }

    @Override
    public boolean containsWord(final String word) {
        return words.contains(word);
    }

    @Override
    public String getName() {
        return name;
    }
}
