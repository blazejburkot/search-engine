package com.example.searchengine.database;

import com.example.searchengine.model.UserQuery;
import com.example.searchengine.rater.ResourceDataRater;
import com.example.searchengine.rater.ResourceDataScore;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class InMemoryDatabase {
    private static final Logger LOGGER = Logger.getLogger(InMemoryDatabase.class.getCanonicalName());

    private final ConcurrentHashMap<String, ResourceData> resourcesData = new ConcurrentHashMap<>();

    public void appendResourceData(final ResourceData resourceData) {
        resourcesData.put(resourceData.getName(), resourceData);
    }

    public Collection<ResourceDataScore> rateResources(final ResourceDataRater rater, final UserQuery query) {
        LOGGER.info("Rating resource: " + query);
        final long startMillis = System.currentTimeMillis();
        try {
            return resourcesData.values().stream()
                    .map(resourceData -> resourceData.accept(rater, query))
                    .collect(Collectors.toSet());
        } finally {
            final long endMillis = System.currentTimeMillis();
            LOGGER.info("Scoring took " + (endMillis - startMillis) + " [ms]");
        }
    }

    public int getNumberOfResources() {
        return resourcesData.size();
    }
}
