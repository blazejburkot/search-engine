package com.example.searchengine.database;

public interface ResourceDataCreator {
    ResourceData create(String resourceName);
}
