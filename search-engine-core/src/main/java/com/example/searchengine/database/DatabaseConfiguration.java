package com.example.searchengine.database;

import com.example.searchengine.config.ApplicationContext;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class DatabaseConfiguration {

    public static void registerBeans(final ApplicationContext context) {
        context.registerBean(InMemoryDatabase.class, new InMemoryDatabase());
        context.registerBean(ResourceDataCreator.class, createResourceDataCreator());
    }

    private static ResourceDataCreator createResourceDataCreator() {
        return ResourceWordsSet::new;
    }
}
