package com.example.searchengine.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Path;


public class FileDataStream implements DataStream {
    private final BufferedReader bufferedReader;

    private String nextLine;

    public FileDataStream(final Path filePath, final Charset charset) {
        try {
            bufferedReader = new BufferedReader(new FileReader(filePath.toFile(), charset));
            nextLine = bufferedReader.readLine();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public String readNextLine() {
        try {
            final String line = nextLine;
            if (line != null) {
                nextLine = bufferedReader.readLine();
            }
            return line;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @Override
    public String peekNextLine() {
        return nextLine;
    }

    @Override
    public void close() {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
