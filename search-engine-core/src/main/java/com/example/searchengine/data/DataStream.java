package com.example.searchengine.data;

public interface DataStream {
    String readNextLine();
    String peekNextLine();
    void close();
}
