package com.example.searchengine.data;

import java.util.ArrayList;
import java.util.List;

public class SimpleDataStream implements DataStream {
    private final List<String> lines;
    private int index;

    public SimpleDataStream(final List<String> lines) {
        this.lines = new ArrayList<>(lines);
        this.index = 0;
    }

    @Override
    public String readNextLine() {
        if (index >= lines.size()) {
            return null;
        }

        return lines.get(index++);
    }

    @Override
    public String peekNextLine() {
        if (index >= lines.size()) {
            return null;
        }

        return lines.get(index);
    }

    @Override
    public void close() {
    }
}
