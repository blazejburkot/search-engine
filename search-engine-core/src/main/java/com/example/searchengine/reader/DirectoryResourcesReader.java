package com.example.searchengine.reader;

import com.example.searchengine.data.DataStream;
import com.example.searchengine.data.FileDataStream;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class DirectoryResourcesReader {
    private static final Logger LOGGER = Logger.getLogger(DirectoryResourcesReader.class.getCanonicalName());

    private final Charset fileCharset;

    public DirectoryResourcesReader(final Charset fileCharset) {
        this.fileCharset = fileCharset;
    }

    public Map<String, DataStream> readResources(final String directoryPath) {
        LOGGER.info("Reading files from dir: " + directoryPath);

        final Path directory = Paths.get(directoryPath);
        assertDirectoryExists(directory);

        try {
            return Files.walk(directory)
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toMap(filePath -> getResourceName(directory, filePath), this::toDataStream));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void assertDirectoryExists(final Path directory) {
        if (Files.notExists(directory)) {
            throw new IllegalArgumentException("Given directory does not exist: "
                    + directory.toAbsolutePath().toString());
        }
        if (!Files.isDirectory(directory)) {
            throw new IllegalArgumentException("Given path is not a directory");
        }
    }

    private String getResourceName(final Path directory, final Path filePath) {
        final int directoryPathLength = directory.toString().length();
        return filePath.toString()
                .substring(directoryPathLength + 1);
    }

    private FileDataStream toDataStream(final Path filePath) {
        LOGGER.fine("Found file: " + filePath);
        return new FileDataStream(filePath, fileCharset);
    }
}
