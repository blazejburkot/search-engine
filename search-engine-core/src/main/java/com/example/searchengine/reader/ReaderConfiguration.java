package com.example.searchengine.reader;

import com.example.searchengine.config.ApplicationContext;
import com.example.searchengine.database.ResourceDataCreator;
import com.example.searchengine.normalizer.WordNormalizer;
import com.example.searchengine.tokenizer.WordTokenizer;

import java.nio.charset.Charset;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class ReaderConfiguration {

    public static void registerBeans(final ApplicationContext context) {
        final InputDataReader inputDataReader = createInputDataReader(context);

        context.registerBean(InputDataReader.class, inputDataReader);
    }

    private static InputDataReader createInputDataReader(final ApplicationContext context) {
        final DirectoryResourcesReader resourcesReader = createResourcesReader(context);
        final ResourceDataCreator dataCreator = context.getBean(ResourceDataCreator.class);
        final WordTokenizer tokenizer = context.getBean(WordTokenizer.class);
        final WordNormalizer normalizer = context.getBean(WordNormalizer.class);

        return new InputDataReader(resourcesReader, dataCreator, tokenizer, normalizer);
    }

    private static DirectoryResourcesReader createResourcesReader(final ApplicationContext context) {
        final String encoding = context.getProperty("data.encoding");
        final Charset charset = Charset.forName(encoding);
        return new DirectoryResourcesReader(charset);
    }
}
