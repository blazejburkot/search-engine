package com.example.searchengine.reader;

import com.example.searchengine.data.DataStream;
import com.example.searchengine.database.InMemoryDatabase;
import com.example.searchengine.database.ResourceData;
import com.example.searchengine.database.ResourceDataCreator;
import com.example.searchengine.normalizer.WordNormalizer;
import com.example.searchengine.tokenizer.WordTokenizer;

import java.util.Iterator;
import java.util.logging.Logger;

public class InputDataReader {
    private static final Logger LOGGER = Logger.getLogger(InMemoryDatabase.class.getCanonicalName());

    private final DirectoryResourcesReader directoryResourcesReader;
    private final ResourceDataCreator resourceDataCreator;
    private final WordTokenizer tokenizer;
    private final WordNormalizer normalizer;


    public InputDataReader(final DirectoryResourcesReader directoryResourcesReader,
                           final ResourceDataCreator resourceDataCreator, final WordTokenizer tokenizer,
                           final WordNormalizer normalizer) {
        this.directoryResourcesReader = directoryResourcesReader;
        this.resourceDataCreator = resourceDataCreator;
        this.tokenizer = tokenizer;
        this.normalizer = normalizer;
    }

    public void populateDatabase(final InMemoryDatabase database, final String resourcePath) {
        final long startMillis = System.currentTimeMillis();
        directoryResourcesReader.readResources(resourcePath)
                .forEach((key, value) -> processResource(database, key, value));
        final long endMillis = System.currentTimeMillis();
        LOGGER.info("Read data in " + (endMillis - startMillis) + " [ms]");
    }

    private void processResource(final InMemoryDatabase database,
                                 final String resourceName, final DataStream dataStream) {
        final ResourceData resourceData = resourceDataCreator.create(resourceName);
        database.appendResourceData(resourceData);

        final Iterator<String> wordIterator = tokenizer.tokenize(dataStream);
        while (wordIterator.hasNext()) {
            final String word = wordIterator.next();
            final String normalizedWord = normalizer.normalize(word);
            resourceData.appendWord(normalizedWord);
        }
        dataStream.close();
    }
}
