package com.example.searchengine.normalizer;

import java.util.List;

public class WordNormalizerComposite implements WordNormalizer {
    private final List<WordNormalizer> normalizers;

    public WordNormalizerComposite(final List<WordNormalizer> normalizers) {
        this.normalizers = List.copyOf(normalizers);
    }

    public static WordNormalizerComposite of(final WordNormalizer... normalizers) {
        return new WordNormalizerComposite(List.of(normalizers));
    }

    @Override
    public String normalize(final String word) {
        String outputValue = word;
        for (WordNormalizer normalizer : normalizers) {
            outputValue = normalizer.normalize(outputValue);
        }
        return outputValue;
    }
}
