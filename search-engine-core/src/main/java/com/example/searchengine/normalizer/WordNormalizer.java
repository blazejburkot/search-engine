package com.example.searchengine.normalizer;

public interface WordNormalizer {
    static WordNormalizer noOp() {
        return value -> value;
    }

    String normalize(String word);
}
