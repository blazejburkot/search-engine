package com.example.searchengine.normalizer;

import java.util.regex.Pattern;

public class InterpunctionCharsNormalizer implements WordNormalizer {
    private static final Pattern INTERRUPTION_REGEX = Pattern.compile("[,.;?!]");

    @Override
    public String normalize(final String word) {
        return INTERRUPTION_REGEX
                .matcher(word)
                .replaceAll("");
    }
}
