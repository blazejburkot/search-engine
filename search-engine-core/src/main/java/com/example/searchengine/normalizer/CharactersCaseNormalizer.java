package com.example.searchengine.normalizer;

public class CharactersCaseNormalizer implements WordNormalizer {

    @Override
    public String normalize(final String word) {
        return word.toLowerCase();
    }
}
