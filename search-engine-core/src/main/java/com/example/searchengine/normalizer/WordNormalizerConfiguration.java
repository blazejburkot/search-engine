package com.example.searchengine.normalizer;

import com.example.searchengine.config.ApplicationContext;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class WordNormalizerConfiguration {

    public static void registerBeans(final ApplicationContext context) {
        final WordNormalizerComposite normalizer =
                WordNormalizerComposite.of(new InterpunctionCharsNormalizer(), new CharactersCaseNormalizer());

        context.registerBean(WordNormalizer.class, normalizer);
    }
}
