package com.example.searchengine.rater;

import com.example.searchengine.database.ResourceData;
import com.example.searchengine.model.UserQuery;

public interface ResourceDataRater {
    int MAX_SCORE = 100;

    ResourceDataScore rate(ResourceData resourceData, UserQuery query);
}
