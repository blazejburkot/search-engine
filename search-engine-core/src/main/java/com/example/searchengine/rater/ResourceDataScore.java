package com.example.searchengine.rater;

public class ResourceDataScore {
    private final String resourceName;
    private final int score;

    public ResourceDataScore(final String resourceName, final int score) {
        this.resourceName = resourceName;
        this.score = score;
    }

    public String getResourceName() {
        return resourceName;
    }

    public int getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "ResourceDataScore{resourceName='" + resourceName + "', score=" + score + '}';
    }
}
