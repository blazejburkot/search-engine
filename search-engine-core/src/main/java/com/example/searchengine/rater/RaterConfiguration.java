package com.example.searchengine.rater;

import com.example.searchengine.config.ApplicationContext;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class RaterConfiguration {

    public static void registerBeans(final ApplicationContext context) {
        context.registerBean(ResourceDataRater.class, new SimpleResourceDataRater());
    }
}
