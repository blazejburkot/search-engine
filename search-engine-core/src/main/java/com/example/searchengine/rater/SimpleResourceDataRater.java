package com.example.searchengine.rater;

import com.example.searchengine.database.ResourceData;
import com.example.searchengine.model.UserQuery;

import java.util.Set;

public class SimpleResourceDataRater implements ResourceDataRater {

    @Override
    public ResourceDataScore rate(final ResourceData resourceData, final UserQuery query) {
        final Set<String> uniqueWords = query.getUniqueWords();

        int foundWord = 0;
        for (String word : uniqueWords) {
            foundWord += resourceData.containsWord(word) ? 1 : 0;
        }

        final int score = calculateScore(foundWord, uniqueWords.size());
        return new ResourceDataScore(resourceData.getName(), score);
    }

    private int calculateScore(final double foundWord, final int uniqueWordsNum) {
        if (uniqueWordsNum == 0) {
            return 0;
        }

        return (int) (foundWord / uniqueWordsNum * MAX_SCORE);
    }
}
