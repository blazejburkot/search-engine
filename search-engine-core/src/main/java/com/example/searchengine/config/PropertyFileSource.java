package com.example.searchengine.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Properties;

public class PropertyFileSource {
    private static final String PROPERTIES_RESOURCE_NAME = "application.properties";

    private final Properties properties = new Properties();

    public void loadProperties() {
        try (InputStream stream =
                     PropertyFileSource.class.getClassLoader().getResourceAsStream(PROPERTIES_RESOURCE_NAME)) {
            properties.load(stream);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public String getProperty(final String propName) {
        return properties.getProperty(propName);
    }
}
