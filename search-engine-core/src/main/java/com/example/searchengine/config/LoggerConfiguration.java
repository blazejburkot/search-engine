package com.example.searchengine.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.logging.LogManager;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class LoggerConfiguration {

    public static void configureLogger(final ApplicationContext context) {
        final String logConfigFile = context.getSystemProperty("java.util.logging.config.file");
        if (logConfigFile != null) {
            return;
        }

        final String debug = context.getProperty("debug");
        final String logConfig = "true".equalsIgnoreCase(debug) ? "logging.properties" : "logging-disabled.properties";

        try (InputStream stream = PropertyFileSource.class.getClassLoader().getResourceAsStream(logConfig)) {
            LogManager.getLogManager().readConfiguration(stream);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
