package com.example.searchengine.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Consumer;

public final class ApplicationContext {
    private final PropertyFileSource propertyFileSource = new PropertyFileSource();
    private final Map<Class<?>, Object> beans = new HashMap<>();

    private ApplicationContext() {
    }

    public static ApplicationContext createContext(final List<Consumer<ApplicationContext>> configurations) {
        final ApplicationContext applicationContext = new ApplicationContext();
        applicationContext.propertyFileSource.loadProperties();

        configurations
                .forEach(config -> config.accept(applicationContext));

        return applicationContext;
    }

    public <T> void registerBean(final Class<T> clazz, final T bean) {
        beans.put(clazz, bean);
    }

    @SuppressWarnings("unchecked")
    public <T> T getBean(final Class<T> clazz) {
        final Object bean = beans.get(clazz);
        if (bean == null) {
            throw new NoSuchElementException("The bean is not defined: " + clazz);
        }

        if (clazz.isAssignableFrom(bean.getClass())) {
            return (T) bean;
        }

        throw new IllegalStateException("Context is corrupted, bean has wrong type");
    }

    public String getProperty(final String propName) {
        final String value = System.getProperty(propName, null);
        if (value != null) {
            return value;
        }
        return propertyFileSource.getProperty(propName);
    }

    public String getSystemProperty(final String propName) {
        return System.getProperty(propName, null);
    }
}
