package com.example.searchengine.config;

import com.example.searchengine.SearchEngineFacade;
import com.example.searchengine.database.DatabaseConfiguration;
import com.example.searchengine.database.InMemoryDatabase;
import com.example.searchengine.normalizer.WordNormalizer;
import com.example.searchengine.normalizer.WordNormalizerConfiguration;
import com.example.searchengine.rater.RaterConfiguration;
import com.example.searchengine.rater.ResourceDataRater;
import com.example.searchengine.reader.InputDataReader;
import com.example.searchengine.reader.ReaderConfiguration;
import com.example.searchengine.tokenizer.WordTokenizer;
import com.example.searchengine.tokenizer.WordTokenizerConfiguration;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class SearchEngineConfiguration {

    public static void configureModule(final ApplicationContext context) {
        DatabaseConfiguration.registerBeans(context);
        RaterConfiguration.registerBeans(context);
        WordNormalizerConfiguration.registerBeans(context);
        WordTokenizerConfiguration.registerBeans(context);
        ReaderConfiguration.registerBeans(context);

        context.registerBean(SearchEngineFacade.class, createSearchEngineFacade(context));
    }

    private static SearchEngineFacade createSearchEngineFacade(final ApplicationContext context) {
        final InputDataReader dataReader = context.getBean(InputDataReader.class);
        final InMemoryDatabase database = context.getBean(InMemoryDatabase.class);
        final ResourceDataRater dataRater = context.getBean(ResourceDataRater.class);
        final WordTokenizer wordTokenizer = context.getBean(WordTokenizer.class);
        final WordNormalizer wordNormalizer = context.getBean(WordNormalizer.class);

        return new SearchEngineFacade(dataReader, database, dataRater, wordTokenizer, wordNormalizer);
    }
}
