package com.example.searchengine;

import com.example.searchengine.data.SimpleDataStream;
import com.example.searchengine.database.InMemoryDatabase;
import com.example.searchengine.model.UserQuery;
import com.example.searchengine.normalizer.WordNormalizer;
import com.example.searchengine.rater.ResourceDataRater;
import com.example.searchengine.rater.ResourceDataScore;
import com.example.searchengine.reader.InputDataReader;
import com.example.searchengine.tokenizer.WordTokenizer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class SearchEngineFacade {
    private final InputDataReader inputDataReader;
    private final InMemoryDatabase database;
    private final ResourceDataRater dataRater;

    private final WordTokenizer tokenizer;
    private final WordNormalizer normalizer;

    public SearchEngineFacade(final InputDataReader inputDataReader,
                              final InMemoryDatabase database,
                              final ResourceDataRater dataRater,
                              final WordTokenizer tokenizer,
                              final WordNormalizer normalizer) {
        this.inputDataReader = inputDataReader;
        this.database = database;
        this.dataRater = dataRater;
        this.tokenizer = tokenizer;
        this.normalizer = normalizer;
    }

    public int getNumberOfResources() {
        return database.getNumberOfResources();
    }

    public void readDataFromDirectory(final String directoryPath) {
        inputDataReader.populateDatabase(database, directoryPath);
    }

    public Collection<ResourceDataScore> rateResources(final String rawQuery) {
        final UserQuery query = toUserQuery(rawQuery);
        return database.rateResources(dataRater, query);
    }

    private UserQuery toUserQuery(final String rawQuery) {
        final List<String> parsedWords = new ArrayList<>();

        final SimpleDataStream dataStream = new SimpleDataStream(List.of(rawQuery));
        final Iterator<String> wordIterator = tokenizer.tokenize(dataStream);
        while (wordIterator.hasNext()) {
            final String word = wordIterator.next();
            final String normalizedWord = normalizer.normalize(word);
            parsedWords.add(normalizedWord);
        }

        return new UserQuery(rawQuery, parsedWords);
    }
}
