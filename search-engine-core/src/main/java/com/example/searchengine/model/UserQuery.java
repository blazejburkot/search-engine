package com.example.searchengine.model;

import java.util.List;
import java.util.Set;

public class UserQuery {
    private final String rawQuery;
    private final List<String> parsedQuery;
    private final Set<String> uniqueWords;

    public UserQuery(final String rawQuery, final List<String> parsedQuery) {
        this.rawQuery = rawQuery;
        this.parsedQuery = List.copyOf(parsedQuery);
        this.uniqueWords = Set.copyOf(parsedQuery);
    }

    public String getRawQuery() {
        return rawQuery;
    }

    public List<String> getParsedQuery() {
        return parsedQuery;
    }

    public Set<String> getUniqueWords() {
        return uniqueWords;
    }

    @Override
    public String toString() {
        return "UserInputQuery{rawQuery='" + rawQuery + "'}";
    }
}
