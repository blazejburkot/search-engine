package com.example.searchengine.tokenizer;

import com.example.searchengine.config.ApplicationContext;

@SuppressWarnings("checkstyle:hideutilityclassconstructor")
public class WordTokenizerConfiguration {

    public static void registerBeans(final ApplicationContext context) {
        context.registerBean(WordTokenizer.class, new WhitespaceWordTokenizer());
    }
}
