package com.example.searchengine.tokenizer;

import com.example.searchengine.data.DataStream;

import java.util.Iterator;

public interface WordTokenizer {
    Iterator<String> tokenize(DataStream dataStream);
}
