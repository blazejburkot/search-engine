package com.example.searchengine.tokenizer;

import com.example.searchengine.data.DataStream;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class WhitespaceWordTokenizer implements WordTokenizer {
    private static final String WHITESPACE_REGEX = "\\s+";

    @Override
    public Iterator<String> tokenize(final DataStream dataStream) {
        return new WordIterator(dataStream);
    }


    private static final class WordIterator implements Iterator<String> {
        private final DataStream dataStream;

        private String[] splittedLine;
        private int index;

        private WordIterator(final DataStream dataStream) {
            this.dataStream = dataStream;

            readNextLine();
        }

        @Override
        public boolean hasNext() {
            if (index < 0) {
                return false;
            } else if (index < splittedLine.length) {
                return true;
            }

            readNextLine();

            return index >= 0;
        }

        @Override
        public String next() {
            if (index < 0) {
                throw new NoSuchElementException();
            }

            return splittedLine[index++];
        }

        private void readNextLine() {
            final String line = readNotEmptyLine();

            if (line == null) {
                splittedLine = new String[]{};
                index = -1;
                return;
            }

            splittedLine = line.split(WHITESPACE_REGEX);
            index = 0;
        }

        private String readNotEmptyLine() {
            String line;
            do {
                line = dataStream.readNextLine();
            }
            while (line != null && line.trim().length() == 0);

            return line;
        }
    }
}
