package com.example.searchengine.config;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class ApplicationContextTest {

    @Test
    public void shouldReturnResistedBean() {
        // given
        final Object bean = Integer.MAX_VALUE;
        final ApplicationContext ctx = ApplicationContext.createContext(Collections.emptyList());
        // when
        ctx.registerBean(Object.class, bean);
        final Object ctxBean = ctx.getBean(Object.class);
        // then
        assertThat(ctxBean).isSameAs(bean);
    }

    @Test
    public void shouldThrowExceptionWhenBeanIsMissing() {
        final ApplicationContext ctx = ApplicationContext.createContext(Collections.emptyList());

        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> ctx.getBean(Integer.class));
    }

    @Test
    public void shouldReturnPropertyValueDefinedInCmd() {
        final String propName = "data.encoding";
        final String propValue = "US_ASCII";

        String ctxProperty = null;
        try {
            System.setProperty(propName, propValue);
            final ApplicationContext ctx = ApplicationContext.createContext(Collections.emptyList());
            ctxProperty = ctx.getProperty(propName);
        } finally {
            System.clearProperty(propValue);
        }

        assertThat(ctxProperty).isEqualTo(propValue);
    }

    @Test
    public void shouldReadPropertyFromPropertyFile() {
        // given
        final String propName = "data.encoding";
        final ApplicationContext ctx = ApplicationContext.createContext(Collections.emptyList());
        // when
        final String ctxProperty = ctx.getProperty(propName);
        // then
        assertThat(ctxProperty).isEqualTo("UTF-8");
    }
}
