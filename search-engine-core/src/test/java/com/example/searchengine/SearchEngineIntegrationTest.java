package com.example.searchengine;

import com.example.searchengine.config.ApplicationContext;
import com.example.searchengine.config.SearchEngineConfiguration;
import com.example.searchengine.rater.ResourceDataScore;
import com.example.searchengine.util.FileUtils;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

class SearchEngineIntegrationTest {

    @Test
    public void integrationTest() {
        // given
        final ApplicationContext applicationContext =
                ApplicationContext.createContext(List.of(SearchEngineConfiguration::configureModule));
        final Path dataDirectory = FileUtils.resourceNameToPath("testdata");
        final SearchEngineFacade searchEngineFacade = applicationContext.getBean(SearchEngineFacade.class);
        final Map<String, Integer> expectedRating = Map.of(
                "lorem-ipsum.txt", 0,
                "lorem-ipsum-a1.txt", 0,
                "pan-tadeusz.txt", 0,
                "hamlet.txt", 100
        );

        // when
        searchEngineFacade.readDataFromDirectory(dataDirectory.toAbsolutePath().toString());

        final Collection<ResourceDataScore> queryRatings = searchEngineFacade.rateResources("to be or no to be");

        // then
        final Map<String, Integer> queryRatingsMap = toMap(queryRatings);
        assertThat(queryRatingsMap).containsExactlyInAnyOrderEntriesOf(expectedRating);
    }

    public Map<String, Integer> toMap(final Collection<ResourceDataScore> queryRatings) {
        return queryRatings.stream()
                .collect(Collectors.toMap(ResourceDataScore::getResourceName, ResourceDataScore::getScore));
    }
}
