package com.example.searchengine.rater;

import com.example.searchengine.database.ResourceData;
import com.example.searchengine.model.UserQuery;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SimpleResourceDataRaterTest {
    private final SimpleResourceDataRater rater = new SimpleResourceDataRater();

    @Test
    public void shouldReturn0scoreWhenInputQueryIsEmpty() {
        // given
        final ResourceData resourceData = mock(ResourceData.class);
        when(resourceData.getName()).thenReturn("resource-1");
        final UserQuery query = new UserQuery("", Collections.emptyList());
        // when
        final ResourceDataScore dataScore = rater.rate(resourceData, query);
        // then
        assertThat(dataScore.getResourceName()).isEqualTo(resourceData.getName());
        assertThat(dataScore.getScore()).isEqualTo(0);
    }

    @Test
    public void shouldReturn0scoreWhenThereIsNoMatches() {
        // given
        final ResourceData resourceData = mock(ResourceData.class);
        when(resourceData.getName()).thenReturn("resource-1");
        final UserQuery query = userInputQuery("Ala ma kota");
        // when
        final ResourceDataScore dataScore = rater.rate(resourceData, query);
        // then
        assertThat(dataScore.getResourceName()).isEqualTo(resourceData.getName());
        assertThat(dataScore.getScore()).isEqualTo(0);
    }

    @Test
    public void shouldReturn1000scoreWhenAllMatch() {
        // given
        final ResourceData resourceData = mock(ResourceData.class);
        when(resourceData.getName()).thenReturn("resource-1");
        when(resourceData.containsWord(any())).thenReturn(Boolean.TRUE);
        final UserQuery query = userInputQuery("Ala ma kota");
        // when
        final ResourceDataScore dataScore = rater.rate(resourceData, query);
        // then
        assertThat(dataScore.getResourceName()).isEqualTo(resourceData.getName());
        assertThat(dataScore.getScore()).isEqualTo(100);
    }

    private UserQuery userInputQuery(final String rawQuery) {
        return new UserQuery(rawQuery, List.of(rawQuery.split("\\s+")));
    }
}
