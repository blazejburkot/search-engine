package com.example.searchengine.util;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtils {

    public static Path resourceNameToPath(final String resourceName) {
        try {
            final URL resource = FileUtils.class.getClassLoader().getResource(resourceName);
            return Paths.get(resource.toURI());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
