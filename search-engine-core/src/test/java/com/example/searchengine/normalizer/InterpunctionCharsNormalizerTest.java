package com.example.searchengine.normalizer;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class InterpunctionCharsNormalizerTest {
    private final InterpunctionCharsNormalizer normalizer = new InterpunctionCharsNormalizer();

    @ParameterizedTest
    @CsvSource({"test,test", "te.st,test", "test?,test", "'te,st',test", "test!,test", "test;,test", "t-est,t-est"})
    public void dataDrivenTest(final String input, final String expected) {
        // when
        final String output = normalizer.normalize(input);
        // then
        assertThat(output).isEqualTo(expected);
    }
}
