package com.example.searchengine.normalizer;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class WordNormalizerCompositeTest {

    @Test
    public void shouldCallComponentsInTheSameOrderAsTheyWherePassedInConstructor() {
        // given
        final WordNormalizer n1 = input -> input + "1";
        final WordNormalizer n2 = input -> input + "2";
        final WordNormalizer n3 = input -> input + "3";
        final WordNormalizerComposite normalizerComposite = WordNormalizerComposite.of(n1, n2, n3);
        // when
        final String normalizedWord = normalizerComposite.normalize("test");
        // then
        assertThat(normalizedWord).isEqualTo("test123");
    }
}
