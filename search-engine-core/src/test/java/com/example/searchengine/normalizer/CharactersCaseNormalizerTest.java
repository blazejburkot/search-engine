package com.example.searchengine.normalizer;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.assertj.core.api.Assertions.assertThat;

class CharactersCaseNormalizerTest {
    private final CharactersCaseNormalizer normalizer = new CharactersCaseNormalizer();

    @ParameterizedTest
    @CsvSource({"test,test", "Test,test", "TEST,test"})
    public void dataDrivenTest(final String input, final String expected) {
        // when
        final String output = normalizer.normalize(input);
        // then
        assertThat(output).isEqualTo(expected);
    }
}
