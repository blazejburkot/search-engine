package com.example.searchengine.data;

import com.example.searchengine.util.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class FileDataStreamTest {

    @Test
    public void shouldReadContentOfTheFile() throws IOException {
        // given
        final Path testFile = FileUtils.resourceNameToPath("testdata/lorem-ipsum-a1.txt");
        final List<String> expectedLines = Files.readAllLines(testFile);
        final FileDataStream dataStream = new FileDataStream(testFile, StandardCharsets.UTF_8);
        // when
        final List<String> lines = new ArrayList<>();

        String line;
        while((line = dataStream.readNextLine()) != null) {
            lines.add(line);
        }

        // then
        assertThat(lines).containsExactlyElementsOf(expectedLines);
    }
}
