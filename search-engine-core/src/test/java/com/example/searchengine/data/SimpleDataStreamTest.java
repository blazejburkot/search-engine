package com.example.searchengine.data;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SimpleDataStreamTest {

    @Test
    public void shouldReturnNullWhenThereIsNoLines() {
        // given
        final List<String> lines = Collections.emptyList();
        final SimpleDataStream dataQueue = new SimpleDataStream(lines);
        // when
        final String line1peek = dataQueue.peekNextLine();
        final String line1 = dataQueue.readNextLine();
        // then
        assertThat(line1).isNull();
        assertThat(line1peek).isNull();
    }

    @Test
    public void shouldReturnGivenLines() {
        // given
        final List<String> lines = List.of("s1", "s2", "s3");
        final SimpleDataStream dataQueue = new SimpleDataStream(lines);
        // when
        final String line1 = dataQueue.readNextLine();
        final String line2 = dataQueue.readNextLine();
        final String line3 = dataQueue.readNextLine();
        final String line4 = dataQueue.readNextLine();
        // then
        assertThat(Arrays.asList(line1, line2, line3)).containsExactlyElementsOf(lines);
        assertThat(line4).isNull();
    }

    @Test
    public void shouldNotRemoveElementFromTheQueue() {
        // given
        final List<String> lines = List.of("s1", "s2", "s3");
        final SimpleDataStream dataQueue = new SimpleDataStream(lines);
        // when
        final String line1peek = dataQueue.peekNextLine();
        final String line1 = dataQueue.readNextLine();
        // then
        assertThat(line1peek).isEqualTo(line1);
    }
}
