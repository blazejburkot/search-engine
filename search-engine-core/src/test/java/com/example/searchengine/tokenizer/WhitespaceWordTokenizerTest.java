package com.example.searchengine.tokenizer;

import com.example.searchengine.data.SimpleDataStream;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class WhitespaceWordTokenizerTest {
    private final WhitespaceWordTokenizer tokenizer = new WhitespaceWordTokenizer();

    @Test
    public void shouldTokenizeWords() {
        // given
        final SimpleDataStream dataQueue = new SimpleDataStream(List.of(
                "To be, or not to be: that is the question:", "Whether 'tis nobler in the mind to suffer"));
        // when
        final List<String> words = Lists.newArrayList(tokenizer.tokenize(dataQueue));
        // then
        assertThat(words).containsExactly("To", "be,", "or", "not", "to", "be:", "that", "is", "the",
                "question:", "Whether", "'tis", "nobler", "in", "the", "mind", "to", "suffer");
    }

    @Test
    public void shouldIgnoreEmptyLines() {
        // given
        final SimpleDataStream dataQueue = new SimpleDataStream(List.of("line 1", "", "line 3"));
        // when
        final List<String> words = Lists.newArrayList(tokenizer.tokenize(dataQueue));
        // then
        assertThat(words).containsExactly("line", "1", "line", "3");
    }

    @Test
    public void shouldReturnEmptyIteratorWhenEmptyLineIsPassed() {
        // given
        final SimpleDataStream dataQueue = new SimpleDataStream(List.of(""));
        // when
        final Iterator<String> wordsItr = tokenizer.tokenize(dataQueue);
        // then
        assertThat(wordsItr.hasNext()).isFalse();
    }

    @Test
    public void shouldReturnEmptyIteratorWhenThereIsNotLinesOnInput() {
        // given
        final SimpleDataStream dataQueue = new SimpleDataStream(Collections.emptyList());
        // when
        final Iterator<String> wordsItr = tokenizer.tokenize(dataQueue);
        // then
        assertThat(wordsItr.hasNext()).isFalse();
    }

    @Test
    public void shouldThrowNoSuchElementExceptionWhenWordIteratorIsMisused() {
        // given
        final SimpleDataStream dataQueue = new SimpleDataStream(Collections.emptyList());
        // when
        final Iterator<String> wordsItr = tokenizer.tokenize(dataQueue);
        // then
        assertThat(wordsItr.hasNext()).isFalse();
        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(wordsItr::next);

    }
}
