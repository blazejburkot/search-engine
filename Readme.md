# Search Engine

---

## Overall information

Key technologies used in this project:
- [Java 12](https://openjdk.java.net/projects/jdk/12/)
- [Maven](https://maven.apache.org/guides/getting-started/)

Import `.editorconfig` ([doc](https://editorconfig.org/)) settings to your IDE before making any changes

Use `mvn test` to execute tests.

## Run Application

1. Build runnable jar -> `mvn clean package`
1. Run Application, execute in root directory
    ```
    java -jar console-application/target/SimpleSearch-jar-with-dependencies.jar <data directory path>
    ```

---

## Future improvements

1. Consider using Apache Lucene as an in-memory database
1. Implement counting word occurrence database. Eg. when user type query `to be or not to be`, he expects the word 'be' to be present at least two times in the resource
1. Implement *Word Tokenizer* which is able to join word which starts in spread in two lines   
1. Implement more sophisticated *Word Normalizer*, check out Apache Lucene's Filters
